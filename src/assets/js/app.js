window._ = require('lodash');

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}


require('icheck');
require('@coreui/coreui/dist/js/coreui');

require('datatables.net-bs4');
require('datatables.net-responsive-bs4');

window.toastr = require('toastr');