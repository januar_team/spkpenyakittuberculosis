<?php


namespace App\Model\Auth;


use App\Model\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use Authenticatable;
}