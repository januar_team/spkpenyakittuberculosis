<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Relations\Pivot;

class DiseaseSymptom extends Pivot
{
    protected $table = 'disease_symptoms';

    public function disease(){
        return $this->belongsTo(Disease::class);
    }

    public function symptom(){
        return $this->belongsTo(Symptom::class);
    }
}
