<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Symptom extends Model
{
    protected $fillable = ['code', 'name'];

    public $probability_hypothesis = 0;

    public $probability_hypothesis_if_evidence = 0;

    protected $appends = ['probability_hypothesis', 'probability_hypothesis_if_evidence'];

    public function diseases(){
        return $this->belongsToMany(Disease::class, (new DiseaseSymptom())->table)
            ->using(DiseaseSymptom::class);
    }

    public function results(){
        return $this->belongsToMany(Result::class, 'result_symptoms');
    }

    public function getProbabilityHypothesisAttribute(){
        return $this->probability_hypothesis;
    }

    public function getProbabilityHypothesisIfEvidenceAttribute(){
        return $this->probability_hypothesis_if_evidence;
    }
}
