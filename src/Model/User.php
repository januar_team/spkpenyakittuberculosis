<?php

namespace App\Model;


use App\Model\Auth\User as UserAlias;

class User extends UserAlias
{

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];
}
