<?php

namespace App\Middleware;


class AuthMiddleware
{
    /**
     * @var array
     */
    private $config;

    /**
     * @var \MY_Router
     */
    private $route;

    /**
     * @var \Auth
    */
    private $auth;

    /**
     * @var \CI_Controller
    */
    private $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->config->load('auth');
        $this->config =& $this->ci->config->config['auth'];
        $this->route =& $this->ci->router;

        $this->ci->load->library('auth');
        $this->auth =& $this->ci->auth;
    }

    public function handle()
    {
        if (!in_array($this->route->getRoute(), $this->config['allowed_route'])) {
            // check user session(authorization)
            if(!$this->auth->isLogin()){
                redirect($this->config['redirect_unauthenticated'], 'location');
            }else{
                if (!$this->auth->isRole('superadmin')){
                    $patern = implode("|", $this->config['superadmin_route']);
                    if (preg_match("/$patern/", $this->route->getRoute())){
                        show_error('Access Denied', 401);
                    }
                }
            }
        } else if ($this->route->getRoute() === $this->config['redirect_unauthenticated']){
            if($this->auth->isLogin()){
                redirect($this->config['redirect_authenticated'], 'location');
            }
        }
    }
}
