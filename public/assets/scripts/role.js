jQuery(document).ready(function(){
    var table = $('#dataTables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        bFilter: true,
        lengthChange: true,
        ajax: {
            url: "/role",
            type: 'POST',
        },
        columns: [
            {
                data: null,
                className:'control',
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
                "width": "20px",
                "orderable": false,
            },
            {
                data: 'disease.code',
            },
            {
                data: 'disease.name',
            },
            {
                data: 'symptom.code',
            },
            {
                data: 'symptom.name',
            },
            {
                data: 'probability',
            },
            {
                width: '15%',
                data: null,
                orderable: false,
                className: 'text-right',
                render: function (data, type, row) {
                    return  "<a class='mb-2 mr-2 btn-transition btn btn-outline-primary btn-sm' href='/role/edit/"+data.disease_id+"/"+ data.symptom_id +"' title='Sunting'><i class='fa fa-pencil'></i></a>" +
                        "<button class='btn-delete mb-2 mr-2 btn-transition btn btn-outline-danger btn-sm' data-item='"+JSON.stringify(data)+"' title='Hapus'><i class='fa fa-trash'></i></button>";
                }
            }
        ],
        columnDefs: [
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: 6},
            { responsivePriority: 3, targets: 5},
            { responsivePriority: 4, targets: 2},
            { responsivePriority: 5, targets: 4},
            { responsivePriority: 6, targets: 1},
            { responsivePriority: 7, targets: 3},
        ]
    });

    $('#dataTables').on('click', 'button.btn-delete', function(event){
       event.preventDefault();
       var data = $(this).data('item');
       $('#btn-delete').data('item', data);
       $('#modal-delete').modal('toggle');
    });

    $('#btn-delete').click(function(){
        var data = $(this).data('item');
        $.ajax({
            url : '/role/delete/' + data.disease_id + '/' + data.symptom_id,
            method : 'POST',
            success : function(response){
                if (response.success){
                    toastr.success("Data have been deleted", 'Success');
                    $('#modal-delete').modal('toggle');
                    table.draw();
                } else{
                    toastr.error(response.message, 'Error');
                }
            },
            error : function(xhr){
                toastr.error(xhr.statusText, 'Error');
            }
        });
    })
});