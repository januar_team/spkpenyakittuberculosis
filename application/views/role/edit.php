<!-- Breadcrumb-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
        <a href="#">Master Data</a>
    </li>
    <li class="breadcrumb-item">
        <a href="/role">Basis Aturan</a>
    </li>
    <li class="breadcrumb-item active">Edit</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <form method="post">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i>Daftar Basis Aturan
                </div>
                <div class="card-body">
                    <div class="position-relative row form-group">
                        <label for="disease_id" class="col-sm-2 col-form-label">Nama Penyakit</label>
                        <div class="col-sm-4">
                            <select name="disease_id" id="disease_id" disabled
                                    class="form-control <?php echo(form_error('disease_id')? 'is-invalid' : '')?>">
                                <?php foreach ($diseases as $disease) { ?>
                                    <option value="<?php echo $disease->id?>"
                                        <?php echo(set_value('disease_id', $role->disease_id) == $disease->id ? "selected" : "") ?>><?php echo $disease->name?></option>
                                <?php } ?>
                            </select>
                            <?php if (form_error('disease_id')){ ?>
                                <div class="invalid-feedback">
                                    <?php echo form_error('disease_id')?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="position-relative row form-group">
                        <label for="symptom_id" class="col-sm-2 col-form-label">Gejala</label>
                        <div class="col-sm-6">
                            <select name="symptom_id" id="symptom_id" disabled
                                    class="form-control <?php echo(form_error('symptom_id')? 'is-invalid' : '')?>">
                                <?php foreach ($symptoms as $symptom) { ?>
                                    <option value="<?php echo $symptom->id?>"
                                        <?php echo(set_value('symptom_id', $role->symptom_id) == $symptom->id ? "selected" : "") ?>><?php echo $symptom->name?></option>
                                <?php } ?>
                            </select>
                            <?php if (form_error('symptom_id')){ ?>
                                <div class="invalid-feedback">
                                    <?php echo form_error('symptom_id')?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="position-relative row form-group">
                        <label for="probability" class="col-sm-2 col-form-label">Nilai Probabilitas</label>
                        <div class="col-sm-4">
                            <input type="number" min="0" max="1" step="0.01"
                                   name="probability" id="probability" placeholder="Nilai Probabilitas"
                                   class="form-control <?php echo(form_error('probability')? 'is-invalid' : '')?>"
                                   value="<?php echo set_value('probability', $role->probability)?>">
                            <?php if (form_error('probability')){ ?>
                                <div class="invalid-feedback">
                                    <?php echo form_error('probability')?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="btn-actions-pane-left">
                        <a class="btn btn-light" href="/role">Batal</a>
                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
    </div>
</div>