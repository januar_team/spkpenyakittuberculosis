<!-- Breadcrumb-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
        <a href="#">Master Data</a>
    </li>
    <li class="breadcrumb-item active">Basis Aturan</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-card mb-3 card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>Basis Aturan

                        <div class="card-header-actions">
                            <a href="/role/add" class="btn btn-primary">add</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="dataTables" width="100%" class="table datatable dataTable table-responsive-sm table-striped nowrap">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Kode Penyakit</th>
                                <th>Nama Penyakit</th>
                                <th>Kode Gejala</th>
                                <th>Gejala</th>
                                <th>Nilai</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Small modal -->

<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah Anda yakin ingin menghapus item</p>
                <p><b id="modal-delete-description"></b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button id="btn-delete" type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>