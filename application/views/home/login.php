<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo env('APP_NAME') ?> - Login</title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"
    />
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <link href="/assets/css/app.css" rel="stylesheet">
</head>

<body>
<div class="app flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card-group">
                    <div class="card p-4">
                        <div class="card-body">
                            <h1>Login</h1>
                            <p class="text-muted"><?php env('APP_NAME') ?></p>
                            <?php echo form_open('/home/login') ?>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="icon-user"></i>
                                    </span>
                                </div>
                                <input class="form-control <?php echo(form_error('username') ? 'is-invalid' : '') ?>"
                                       type="text" placeholder="Username" name="username"
                                       value="<?php echo set_value('username') ?>">
                                <?php if (form_error('username')) { ?>
                                    <div class="invalid-feedback">
                                        <?php echo form_error('username') ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="input-group mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="icon-lock"></i>
                                    </span>
                                </div>
                                <input name="password"
                                       value="<?php echo set_value('password') ?>"
                                       placeholder="Password here..."
                                       type="password"
                                       class="form-control <?php echo(form_error('password') ? 'is-invalid' : '') ?>">
                                <?php if (form_error('password')) { ?>
                                    <div class="invalid-feedback">
                                        <?php echo form_error('password') ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button class="btn btn-primary px-4" type="submit">Login</button>
                                </div>
                                <div class="col-6 text-right">
                                </div>
                            </div>
                            <?php echo form_close() ?>
                        </div>
                    </div>
                    <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
                        <div class="card-body text-center">
                            <div>
                                <h2><?php echo env('APP_NAME')?></h2>
                                <p>Copyright © <?php echo env('APP_AUTHOR')?> 2019</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/assets/scripts/app.js"></script>
</body>
</html>
