<!-- Breadcrumb-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item active">Dashboard</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12 col-xl-12">
                <div class="main-card mb-3 card">
                    <div class="card-header"><i class="fa fa-align-justify"></i>Penyakit Tuberculosis</div>
                    <div class="card-body">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xl-12">
                <div class="main-card mb-3 card">
                    <div class="card-header"><i class="fa fa-align-justify"></i>Metode Teorema Bayes</div>
                    <div class="card-body">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="mb-3 card">
                    <div class="card-header-tab card-header-tab-animation card-header">
                        <i class="fa fa-align-justify"></i>
                            History Diagnosa
                    </div>
                    <div class="card-body">
                        <table id="dataTables" class="table table-responsive-sm" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Pasien</th>
                                <th>Gejala</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($result as $key => $item) {
                                ?>
                                <tr>
                                    <td><?php echo ($key+1)?></td>
                                    <td><?php echo $item->patient ?></td>
                                    <td>
                                        <ul>
                                            <?php
                                            $temp = [];
                                            foreach ($item->data as $data) {
                                                foreach ($data['symptoms'] as $gejala) {
                                                    if (!in_array($gejala['id'], $temp))
                                                        $temp[] = $gejala['id'];
                                                    else
                                                        continue;
                                                    ?>
                                                    <li><?php echo $gejala['name'] ?></li>
                                                <?php }
                                            }?>
                                        </ul>
                                    </td>
                                    <td>
                                        <a class='mb-2 mr-2 btn-transition btn btn-outline-primary btn-sm'
                                           href='/diagnosa/result/<?php echo $item->id?>' title='Sunting'><i class='icons cui-align-center'></i></a>
                                    </td>
                                </tr>
                                <?php
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>