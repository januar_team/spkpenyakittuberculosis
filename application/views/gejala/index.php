<!-- Breadcrumb-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
        <a href="#">Mater Data</a>
    </li>
    <li class="breadcrumb-item active">Daftar Gejala Penyakit</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn"><div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i>Daftar Gejala Penyakit

                <div class="card-header-actions">
                    <a href="/gejala/add" class="btn btn-primary">add</a>
                </div>
            </div>
            <div class="card-body">
                <table id="dataTables" class="table table-striped datatable dataTable table-responsive-sm nowrap" width="100%">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Kode</th>
                        <th>Gejala</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
    </div>
</div>

<!-- Small modal -->

<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah Anda yakin ingin menghapus item</p>
                <p><b id="modal-delete-description"></b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button id="btn-delete" type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>