<!-- Breadcrumb-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
        <a href="#">Master Data</a>
    </li>
    <li class="breadcrumb-item">
        <a href="/disease">Daftar Penyakit</a>
    </li>
    <li class="breadcrumb-item active">Edit</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-card mb-3 card">
                    <form method="post">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>Daftar Penyakit
                        </div>
                        <div class="card-body">
                            <div class="position-relative row form-group">
                                <label for="code" class="col-sm-2 col-form-label">Kode</label>
                                <div class="col-sm-4">
                                    <input name="code" id="code" placeholder="Kode" type="text"
                                           class="form-control <?php echo(form_error('code') ? 'is-invalid' : '') ?>"
                                           value="<?php echo set_value('code', $disease->code) ?>">
                                    <?php if (form_error('code')) { ?>
                                        <div class="invalid-feedback">
                                            <?php echo form_error('code') ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <label for="name" class="col-sm-2 col-form-label">Nama Penyakit</label>
                                <div class="col-sm-6">
                                    <input name="name" id="name" placeholder="Nama Penyakit" type="text"
                                           class="form-control <?php echo(form_error('name') ? 'is-invalid' : '') ?>"
                                           value="<?php echo set_value('name', $disease->name) ?>">
                                    <?php if (form_error('name')) { ?>
                                        <div class="invalid-feedback">
                                            <?php echo form_error('name') ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <label for="description" class="col-sm-2 col-form-label">Deskripsi</label>
                                <div class="col-sm-10">
                            <textarea name="description" id="description" placeholder="Deskripsi"
                                      class="form-control"><?php echo set_value('description', $disease->description) ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="btn-actions-pane-left">
                                <a class="btn btn-light" href="/disease">Batal</a>
                                <button class="btn btn-primary" type="submit">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>