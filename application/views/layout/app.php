<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo env('APP_NAME') ?></title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"
    />
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <link href="/assets/css/app.css" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
        <img class="navbar-brand-full" src="/images/brand/logo.svg" width="89" height="25" alt="CoreUI Logo">
        <img class="navbar-brand-minimized" src="/images/brand/sygnet.svg" width="30" height="30" alt="CoreUI Logo">
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="d-md-down-none navbar-nav"><li class="px-3 nav-item"><a class="nav-link" href="#/">Hi, <b><?php echo auth()->getUser()->name?></b></a></li></ul>
    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img class="img-avatar" src="/assets/images/avatars/4.jpg" alt="admin@bootstrapmaster.com">
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong>Account</strong>
                </div>
                <a class="dropdown-item" href="#" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="fa fa-lock"></i> Logout</a>

                <?php echo form_open('/home/logout', ['style' => "display:none", "id" => "logout-form"]) ?>
                <?php echo form_close() ?>
            </div>
        </li>
    </ul>
</header>

<div class="app-body">
    <div class="sidebar">
        <nav class="sidebar-nav">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link <?php echo ($class == 'home' ? 'active' : '')?>" href="/">
                        <i class="nav-icon icon-speedometer"></i> Dashboard
                        <span class="badge badge-primary">NEW</span>
                    </a>
                </li>
                <?php if (auth()->isRole('superadmin')){ ?>
                    <li class="nav-title">Master Data</li>
                    <li class="nav-item">
                        <a href="/disease" class="nav-link <?php echo ($class == 'disease' ? 'active' : '')?>">
                            <i class="nav-icon cui-task"></i> Daftar Penyakit
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/gejala" class="nav-link <?php echo ($class == 'gejala' ? 'active' : '')?>">
                            <i class="nav-icon cui-task"></i> Daftar Gejala Penyakit
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/role" class="nav-link <?php echo ($class == 'role' ? 'active' : '')?>">
                            <i class="nav-icon cui-task"></i> Basis Aturan
                        </a>
                    </li>
                <?php } ?>
                <li class="nav-title">Diagnosa</li>
                <li class="nav-item">
                    <a class="nav-link" href="/diagnosa">
                        <i class="nav-icon icon-calculator"></i>
                        Proses Diagnosa Penyakit</a>
                </li>
                <li class="divider"></li>
            </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>
    <main class="main">
        <?php echo $content ?>
    </main>
</div>

<footer class="app-footer">
    <div>
        <a href="https://coreui.io"><?php echo env('APP_AUTHOR')?></a>
        <span>&copy; 2019.</span>
    </div>
</footer>
<script type="text/javascript" src="/assets/scripts/app.js"></script>
<script type="application/javascript">
    jQuery(document).ready(function() {
        $('input.icheck').iCheck({
            checkboxClass: 'icheckbox_square-red',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        jQuery(document).ready(function(){
            $('.btn-reset').click(function (event) {
                $('input.icheck').iCheck('uncheck');
            })
        })
    });
</script>
<?php if (isset($scripts)){
    foreach ($scripts as $script){
        ?>
        <script type="text/javascript" src="<?php echo $script?>"></script>
        <?php
    }
} ?>
</body>
</html>
