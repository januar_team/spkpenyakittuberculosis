<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$auth['enable'] = true;
$auth['allowed_route'] = [
    '/home/login'
];
$auth['redirect_authenticated'] = '/';
$auth['redirect_unauthenticated'] = '/home/login';

$auth['model'] = \App\Model\User::class;

$auth['superadmin_route'] = [
    '^\/gejala\/',
    '^\/disease\/',
    '^\/role\/',
];

$config['auth'] = $auth;