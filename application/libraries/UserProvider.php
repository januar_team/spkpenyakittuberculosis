<?php


use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Str;

class UserProvider
{
    /**
     * @var \CI_Controller
     */
    private $ci;

    /**
     * @var array
     */
    private $config;

    /**
     * @var string
    */
    private $model;

    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->config->load('auth');
        $this->config =& $this->ci->config->config['auth'];

        $this->model = $this->config['model'];
    }

    public function attempt(array $credentials){
        $user = $this->retrieveByCredentials($credentials);
        return $user;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model
    */
    private function createModel(){
        $class = '\\'.ltrim($this->model, '\\');

        return new $class;
    }

    public function retrieveByCredentials(array $credentials){
        if (empty($credentials) ||
            (count($credentials) === 1 &&
                array_key_exists('password', $credentials))) {
            return;
        }

        $query = $this->createModel()->newQuery();

        foreach ($credentials as $key => $value) {
            if (Str::contains($key, 'password')) {
                continue;
            }

            if (is_array($value) || $value instanceof Arrayable) {
                $query->whereIn($key, $value);
            } else {
                $query->where($key, $value);
            }
        }

        return $query->first();
    }

    public function validateCredentials($user, array $credentials){
        $plain = $credentials['password'];

        return (password_verify($plain, $user->password));
    }

    public function retrieveById($identifier){
        $model = $this->createModel();

        return $model->newQuery()->where($model->getAuthIdentifierName(), $identifier)
            ->first();
    }
}