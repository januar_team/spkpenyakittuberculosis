<?php
defined('BASEPATH') or exit('No direct script access allowed');
// Don't forget include/define REST_Controller path

/**
 *
 * Controller Home
 *
 * This controller for ...
 *
 * @package   CodeIgniter
 * @category  Controller CI
 * @param     ...
 * @return    ...
 *
 */

class Home extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
        $scripts = [
            '/assets/scripts/home.js'
        ];
        $response['scripts'] = $scripts;

        $response['result'] = \App\Model\Result::orderBy('id', 'DESC')->get();

		$this->view->load($response);
	}

	public function login(){
	    if ($this->isPost()){
	        $this->auth->login();
        }

	    $this->view->load([], ['use_template' => false]);
    }

    public function logout(){
	    if ($this->isPost()){
	        $this->auth->logout();
        }

	    show_404();
    }
}


/* End of file Homephp */
/* Location: ./application/controllers/Homephp */
