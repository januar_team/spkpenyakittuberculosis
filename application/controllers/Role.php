<?php


use App\Model\DiseaseSymptom;
use App\Model\Symptom;

class Role extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $response = array();
        if ($this->isPost()){
            $search = '';
            $start = $this->input->post('start');
            $length = $this->input->post('length');

            if( !empty($this->input->post('search')) )
                $search = $this->input->post('search')['value'];
            else
                $search = null;

            $column = [
                "diseases.code",
                "diseases.name",
                "symptoms.code",
                "symptoms.name",
            ];

            $query = DiseaseSymptom::with(['symptom' ,'disease'])
            ->join('diseases', 'diseases.id', '=', 'disease_id')
            ->join('symptoms', 'symptoms.id', '=', 'symptom_id')
                ->where('symptoms.code', 'LIKE', "%$search%")
                ->orWhere('symptoms.name', 'LIKE', "%$search%")
                ->orWhere('diseases.code', 'LIKE', "%$search%")
                ->orWhere('diseases.name', 'LIKE', "%$search%");

            $total = $query->count();
            $data = $query->orderBy($column[$this->input->post('order')[0]['column']],
                $this->input->post('order')[0]['dir'] )
                ->select('disease_symptoms.*')
                ->skip($start)
                ->take($length)
                ->get();

            $response = [
                'data' => $data,
                'draw' => intval($this->input->post('draw')),
                'recordsTotal' => $total,
                'recordsFiltered' => $total
            ];

            return $this->json($response);
        }

        $scripts = [
            '/assets/scripts/role.js'
        ];
        $response['scripts'] = $scripts;
        $this->view->load($response);
    }

    public function add(){
        $response = array();
        if ($this->isPost()){
            $this->form_validation->set_rules('disease_id', 'Nama Penyakit', 'required');
            $this->form_validation->set_rules('symptom_id', 'Gejala', 'required');
            $this->form_validation->set_rules('probability', 'Nilai Probabilitas', 'required|greater_than[0]|less_than[1]');
            if ($this->form_validation->run()){
                if (DiseaseSymptom::with(['disease','symptom'])->where('disease_id', $this->input->post('disease_id'))
                    ->where('symptom_id', $this->input->post('symptom_id'))->first()){
                    $this->form_validation->set_field_data('disease_id', [
                        'field'		=> 'disease_id',
                        'label'		=> 'Nama Penyakit',
                        'rules'		=> 'exits',
                        'errors'	=> '',
                        'is_array'	=> false,
                        'keys'		=> 'disease_id',
                        'postdata'	=> $this->input->post('disease_id'),
                        'error'		=> 'This item already exists.'
                    ]);
                }else {
                    $disease = \App\Model\Disease::find($this->input->post('disease_id'));
                    $disease->symptoms()->attach($this->input->post('symptom_id'),
                        ['probability' => $this->input->post('probability')]);

                    redirect('role');
                }
            }
        }

        $response['diseases'] = \App\Model\Disease::all();
        $response['symptoms'] = Symptom::all();

        $this->view->load($response);
    }

    public function edit($disease_id, $symptom_id){
        $response = array();
        $role = DiseaseSymptom::with(['disease','symptom'])->where('disease_id', $disease_id)
            ->where('symptom_id', $symptom_id)->first();
        if (!$role){
            redirect('/role');
        }

        if ($this->isPost()){
            $this->form_validation->set_rules('probability', 'Nilai Probabilitas', 'required|greater_than[0]|less_than[1]');
            if ($this->form_validation->run()){
                $disease = \App\Model\Disease::find($disease_id);
                $disease->symptoms()->updateExistingPivot($symptom_id,
                    ['probability' => $this->input->post('probability')]);

                redirect('role');
            }
        }

        $response['diseases'] = \App\Model\Disease::all();
        $response['symptoms'] = Symptom::all();
        $response['role'] = $role;

        $this->view->load($response);
    }

    public function delete($disease_id, $symptom_id){
        $response = [
            'success' => false,
            'message' => ''
        ];

        if ($this->isPost()){
            $disease = \App\Model\Disease::find($disease_id);
            $disease->symptoms()->detach([$symptom_id]);

            $response['success'] = true;
        }else{
            $response['message'] = 'Method not allowed!';
        }

        $this->json($response);
    }
}