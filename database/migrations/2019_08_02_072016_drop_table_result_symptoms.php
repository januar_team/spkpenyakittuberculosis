<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTableResultSymptoms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('result_symptoms');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('result_symptoms', function (Blueprint $table){
            $table->unsignedBigInteger('result_id');
            $table->unsignedBigInteger('symptom_id');

            $table->foreign('result_id')->references('id')->on('results');
            $table->foreign('symptom_id')->references('id')->on('symptoms');
        });
    }
}
