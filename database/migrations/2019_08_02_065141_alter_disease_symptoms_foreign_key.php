<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDiseaseSymptomsForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('disease_symptoms', function (Blueprint $table) {
            $table->dropForeign('disease_symptoms_disease_id_foreign');
            $table->foreign('disease_id')->references('id')->on('diseases')->onUpdate('cascade')
            ->onDelete('cascade');

            $table->dropForeign('disease_symptoms_symptom_id_foreign');
            $table->foreign('symptom_id')->references('id')->on('symptoms')->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('disease_symptoms', function (Blueprint $table) {
            $table->dropForeign('disease_symptoms_disease_id_foreign');
            $table->foreign('disease_id')->references('id')->on('diseases');
            $table->dropForeign('disease_symptoms_symptom_id_foreign');
            $table->foreign('symptom_id')->references('id')->on('symptoms');
        });
    }
}
